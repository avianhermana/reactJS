export function apiGetListNews() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({
        data: {
          status: 'ok',
          totalResults: 5922,
          articles: [
            {
              source: {id: null, name: 'Detik.com'},
              author: 'Aulia Damayanti',
              title: 'Gokil! Harga Bitcoin Melejit Jadi Rp 254 Juta',
              description:
                'Uang digital, bitcoin melonjak ke level lebih dari US$ 18 ribu setara Rp 254,9 juta (kurs Rp 14.111). harga itu terbesar pertama kali sejak 20 Desember 2017.',
              url:
                'https://finance.detik.com/moneter/d-5261151/gokil-harga-bitcoin-melejit-jadi-rp-254-juta',
              urlToImage:
                'https://awsimages.detik.net.id/api/wm/2018/02/20/341aa4dc-5e5b-44bd-a028-3c186b377044_169.jpeg?wid=54&w=650&v=1&t=jpeg',
              publishedAt: '2020-11-19T03:41:06Z',
              content:
                'Jakarta - Uang digital, bitcoin melonjak ke level lebih dari US$ 18 ribu setara Rp 254,9 juta (kurs Rp 14.111). harga itu terbesar pertama kali sejak 20 Desember 2017. Sepanjang tahun ini bitcoin ter… [+1367 chars]',
            },
            {
              source: {id: null, name: 'Zerohedge.com'},
              author: 'Tyler Durden',
              title:
                'Deutsche: "There Is Increasing Demand To Use Bitcoin Where Gold Was Used To Hedge Dollar Risk And Inflation"',
              description:
                'Deutsche: "There Is Increasing Demand To Use Bitcoin Where Gold Was Used To Hedge Dollar Risk And Inflation"\n\n Tyler Durden\n \nWed, 11/18/2020 - 22:30\n\n From Deutsche Bank\'s Jim Reid\r\n\n\nA divergent world post vaccine news \r\n\n\nThere has been some consolidation …',
              url:
                'https://www.zerohedge.com/markets/deutsche-there-increasing-demand-use-bitcoin-where-gold-was-used-hedge-dollar-risk-and',
              urlToImage:
                'https://zh-prod-1cc738ca-7d3b-4a72-b792-20bd8d8fa069.storage.googleapis.com/s3fs-public/styles/max_650x650/public/2020-11/Gold%20vs%20Bitcoin.jpg?itok=CIDCEDJ4',
              publishedAt: '2020-11-19T03:30:00Z',
              content:
                "From Deutsche Bank's Jim Reid\r\nA divergent world post vaccine news \r\nThere has been some consolidation in the S&amp;P 500 after last weeks vaccine euphoria. The index is 'only' 1% above where we clos… [+1293 chars]",
            },
            {
              source: {id: null, name: 'Yahoo Entertainment'},
              author: 'Wayne Cole',
              title: 'Asia stocks take a breather, bonds bet on Fed action',
              description:
                'Asian shares eased from all-time highs on Thursday as widening COVID-19 restrictions in the United states weighed on Wall Street, while bonds were...',
              url:
                'https://finance.yahoo.com/news/asia-stocks-breather-bonds-bet-032012796.html',
              urlToImage:
                'https://s.yimg.com/uu/api/res/1.2/icB1XzGJLSFSHm7MtIPeUw--~B/aD01MzQ7dz04MDA7YXBwaWQ9eXRhY2h5b24-/https://media.zenfs.com/en-US/reuters.com/f6d48346caa729fd1762ad35247bedfb',
              publishedAt: '2020-11-19T03:20:12Z',
              content:
                'By Wayne Cole\r\nSYDNEY (Reuters) - Asian shares eased from all-time highs on Thursday as widening COVID-19 restrictions in the United states weighed on Wall Street, while bonds were underpinned by spe… [+3149 chars]',
            },
            {
              source: {id: null, name: 'Cointelegraph'},
              author: 'Cointelegraph By Martin Young',
              title:
                'Bitcoin mining revenue hits yearly high, after return to pre-halving levels',
              description:
                'Mining revenue is back at, or better than, pre-halving levels with BTC hitting $18,000 and not slowing down.',
              url:
                'https://cointelegraph.com/news/bitcoin-mining-revenue-hits-yearly-high-after-return-to-pre-halving-levels',
              urlToImage:
                'https://s3.cointelegraph.com/uploads/2020-11/00760dc8-8410-4b7b-96f8-b215f20b887b.jpg',
              publishedAt: '2020-11-19T03:15:41Z',
              content:
                'Key on-chain metrics such as Bitcoin mining revenues have returned to pre-halving levels according to recent research.\r\nData from analytics provider, Glassnode, suggests that revenue from Bitcoin min… [+2173 chars]',
            },
            {
              source: {id: null, name: 'Cointelegraph'},
              author: 'Cointelegraph By Joseph Young',
              title:
                '4 reasons why Bitcoin’s bull run is intact despite a surprise stop hunt',
              description:
                'Technical factors suggest Bitcoin is still in a bull run even after a sharp sell-off from $18,476.',
              url:
                'https://cointelegraph.com/news/4-reasons-why-bitcoin-s-bull-run-is-intact-despite-a-surprise-stop-hunt',
              urlToImage:
                'https://s3.cointelegraph.com/uploads/2020-11/5aa83d2d-974d-49aa-b18f-3ac2063c280b.jpg',
              publishedAt: '2020-11-19T03:08:37Z',
              content:
                'After Bitcoin (BTC) price suddenly dropped from around $18,500 to $17,200, some traders began to question whether a local top had formed but there are multiple factors that suggest the bull run is st… [+3013 chars]',
            },
            {
              source: {id: 'reuters', name: 'Reuters'},
              author: 'Wayne Cole',
              title:
                'Asia stocks take a breather, bonds bet on Fed action - Reuters.com',
              description:
                'Asian shares eased from all-time highs on Thursday as widening COVID-19 restrictions in the United states weighed on Wall Street, while bonds were underpinned by speculation the Federal Reserve would have to respond with yet more easing.',
              url:
                'https://www.reuters.com/article/us-global-markets-idUSKBN27Z00V',
              urlToImage:
                'https://static.reuters.com/resources/r/?m=02&d=20201119&t=2&i=1541697523&r=LYNXMPEGAI00E&w=800',
              publishedAt: '2020-11-19T03:01:22Z',
              content:
                'SYDNEY (Reuters) - Asian shares eased from all-time highs on Thursday as widening COVID-19 restrictions in the United states weighed on Wall Street, while bonds were underpinned by speculation the Fe… [+3233 chars]',
            },
            {
              source: {id: null, name: '13newsnow.com WVEC'},
              author: 'Adriana De Alba',
              title:
                'FBI warns of major online scams ahead of the holiday season',
              description:
                'FBI Norfolk says scammers are taking advantage of the fact that more people will shop for gifts online due to the coronavirus pandemic.',
              url:
                'https://www.13newsnow.com/article/life/shopping/fbi-warns-of-major-online-scams-ahead-of-the-holiday-season/291-763b7a8a-cd14-44ae-9f8b-e394a2a75846',
              urlToImage:
                'https://media.13newsnow.com/assets/WVEC/images/593268798/593268798_1140x641.jpg',
              publishedAt: '2020-11-19T02:59:37Z',
              content:
                'NORFOLK, Va. The holiday season is upon us.\r\nAs people get ready to find gifts for their loved ones, experts say more shoppers will shop for gifts online this year due to the ongoing coronavirus pand… [+2454 chars]',
            },
            {
              source: {id: 'the-times-of-india', name: 'The Times of India'},
              author: 'Reuters',
              title: 'Asia stocks take a breather, bonds bet on Fed action',
              description:
                "MSCI's broadest index of Asia-Pacific shares outside Japan fell 0.5%, though that was from a record peak. Chinese blue chips were a fraction firmer. E-Mini futures for the S&P 500 steadied, after Wall Street took a late dip on Wednesday. The Dow ended down 1.…",
              url:
                'https://economictimes.indiatimes.com/markets/stocks/news/asia-stocks-take-a-breather-bonds-bet-on-fed-action/articleshow/79294824.cms',
              urlToImage:
                'https://img.etimg.com/thumb/msid-79294854,width-1070,height-580,imgsize-586776,overlay-etmarkets/photo.jpg',
              publishedAt: '2020-11-19T02:58:35Z',
              content:
                'SYDNEY: Asian shares eased from all-time highs on Thursday as widening COVID-19 restrictions in the United states weighed on Wall Street, while bonds were underpinned by speculation the Federal Reser… [+3035 chars]',
            },
            {
              source: {id: null, name: 'The Sports Daily'},
              author: 'Kyle Austin',
              title:
                'Crucial tips to follow while using Bitcoin in online sports betting!',
              description:
                'With the rapidly developing Internet technology, digital currency’s popularity, better known as cryptocurrency, is also increasing. The digital currency was used for normal purposes such as b…',
              url:
                'https://thesportsdaily.com/2020/11/18/crucial-tips-to-follow-while-using-bitcoin-in-online-sports-betting/',
              urlToImage:
                'https://thesportsdaily.com/wp-content/uploads/sites/95/2020/11/USATSI_15055700.jpg?w=1024&h=576&crop=1',
              publishedAt: '2020-11-19T02:52:44Z',
              content:
                'With the rapidly developing Internet technology, digital currency’s popularity, better known as cryptocurrency, is also increasing. The digital currency was used for normal purposes such as buying go… [+4449 chars]',
            },
            {
              source: {id: null, name: 'The Sports Daily'},
              author: 'Joe Newberry',
              title: 'How to utilize the bitcoins for sports betting?',
              description:
                'The cryptocurrency has gained popularity in a very short time. No industry has boomed in a way in which the cryptocurrency industry has. The bitcoin system was introduced a decade ago, and they man…',
              url:
                'https://thesportsdaily.com/2020/11/18/how-to-utilize-the-bitcoins-for-sports-betting/',
              urlToImage:
                'https://thesportsdaily.com/wp-content/uploads/sites/95/2020/11/USATSI_14154967.jpg?w=1024&h=576&crop=1',
              publishedAt: '2020-11-19T02:52:41Z',
              content:
                'The cryptocurrency has gained popularity in a very short time. No industry has boomed in a way in which the cryptocurrency industry has. The bitcoin system was introduced a decade ago, and they manag… [+4544 chars]',
            },
            {
              source: {id: null, name: 'The Sports Daily'},
              author: 'Randall Jenkins',
              title: 'An Ultimate Guide of Sports Betting with Bitcoin!',
              description:
                'The power of cryptocurrency has improved numerous industries and sectors. The most popular virtual currency that changed the payment method of almost all the industries is bitcoin. The bitcoin syst…',
              url:
                'https://thesportsdaily.com/2020/11/18/an-ultimate-guide-of-sports-betting-with-bitcoin/',
              urlToImage:
                'https://thesportsdaily.com/wp-content/uploads/sites/95/2020/11/USATSI_14055810.jpg?w=1024&h=576&crop=1',
              publishedAt: '2020-11-19T02:52:36Z',
              content:
                'The power of cryptocurrency has improved numerous industries and sectors. The most popular virtual currency that changed the payment method of almost all the industries is bitcoin. The bitcoin system… [+4581 chars]',
            },
            {
              source: {id: null, name: 'The Sports Daily'},
              author: 'Lisa Clarke',
              title:
                'Sports betting with bitcoins - Offering great convenience!',
              description:
                'With the advancement in Internet technology, many things have been changed, and most of the essential things and activities have been shifted to the Internet. The wave of digitalization has digital…',
              url:
                'https://thesportsdaily.com/2020/11/18/sports-betting-with-bitcoins-offering-great-convenience/',
              urlToImage:
                'https://thesportsdaily.com/wp-content/uploads/sites/95/2020/11/USATSI_13933295.jpg?w=1024&h=576&crop=1',
              publishedAt: '2020-11-19T02:52:32Z',
              content:
                'With the advancement in Internet technology, many things have been changed, and most of the essential things and activities have been shifted to the Internet. The wave of digitalization has digitaliz… [+4474 chars]',
            },
            {
              source: {id: null, name: 'Cointelegraph'},
              author: 'Cointelegraph By Turner Wright',
              title: 'Everything The Felder Report got wrong about Bitcoin',
              description:
                "Jesse Felder, former manager of a billion-dollar hedge fund, stated many misassumptions about Bitcoin's scarcity, security, and effectiveness as a store of value on his blog.",
              url:
                'https://cointelegraph.com/news/everything-the-felder-report-got-wrong-about-bitcoin',
              urlToImage:
                'https://s3.cointelegraph.com/uploads/2020-11/7f5e7b96-df65-4114-81d5-46e28e747e20.jpg',
              publishedAt: '2020-11-19T02:39:46Z',
              content:
                'Bitcoiners are crying foul at former billion-dollar hedge fund manager Jesse Felder\'s "inaccurate" hot take blog post about Bitcoin today in which he claims the crypto asset doesnt make sense as an i… [+3003 chars]',
            },
            {
              source: {id: null, name: 'Independent.ie'},
              author: 'Tom Wilson, Gertrude Chavez-Dreyfus, Adrian Weckler',
              title:
                'Retail investors pump up Bitcoin once again as it climbs to near 4-year high',
              description:
                'Bitcoin has risen to its highest level since December 2017 as the asset’s perceived quality as a hedge against inflation and expectations of mainstream acceptance lured institutional and retail demand.',
              url:
                'https://www.independent.ie/business/technology/retail-investors-pump-up-bitcoin-once-again-as-it-climbs-to-near-4-year-high-39763867.html',
              urlToImage:
                'https://www.independent.ie/business/technology/69fa2/39763866.ece/AUTOCROP/w1240h700/Bitcoin',
              publishedAt: '2020-11-19T02:30:00Z',
              content:
                'Bitcoin has risen to its highest level since December 2017 as the assets perceived quality as a hedge against inflation and expectations of mainstream acceptance lured institutional and retail demand… [+2383 chars]',
            },
            {
              source: {id: 'reuters', name: 'Reuters'},
              author: 'Wayne Cole',
              title:
                'GLOBAL MARKETS-Asia stocks take a breather, bonds bet on Fed action - Reuters India',
              description:
                'Asian shares eased from all-time highs on Thursday as widening COVID-19 restrictions in the United states weighed on Wall Street, while bonds were underpinned by speculation the Federal Reserve would have to respond with yet more easing.',
              url:
                'https://in.reuters.com/article/global-markets-idINKBN27Z0BU',
              urlToImage:
                'https://static.reuters.com/resources/r/?m=02&d=20201119&t=2&i=1541709565&r=LYNXMPEGAI059&w=800',
              publishedAt: '2020-11-19T02:28:00Z',
              content:
                'SYDNEY (Reuters) - Asian shares eased from all-time highs on Thursday as widening COVID-19 restrictions in the United states weighed on Wall Street, while bonds were underpinned by speculation the Fe… [+3305 chars]',
            },
            {
              source: {id: null, name: 'Cointelegraph'},
              author: 'Cointelegraph By Joshua Mapperson',
              title:
                'Survey of millionaires finds 73% own or want to invest in crypto',
              description:
                'Crypto-sentiment among high net worth investors is increasingly bullish, with a new survey revealing that 73% of millionaires already own, or want to buy, digital assets',
              url:
                'https://cointelegraph.com/news/survey-of-millionaires-finds-73-own-or-want-to-invest-in-crypto',
              urlToImage:
                'https://s3.cointelegraph.com/uploads/2020-11/f7206dd1-2d7c-4b60-ae6e-e5beaa8d3666.jpg',
              publishedAt: '2020-11-19T02:11:53Z',
              content:
                'A survey of more than 700 high net worth individuals, or HNWs, has found that almost three-quarters of millionaire respondents either already own or are looking to invest in cryptocurrencies before t… [+2350 chars]',
            },
            {
              source: {id: null, name: 'Yahoo.co.jp'},
              author: 'CoinDesk Japan',
              title:
                'ビットコイン、最高値を更新しても利益確定売りは限定的か：市場予想（CoinDesk Japan）',
              description:
                'ビットコインの価格が過去数日にわたり上昇した間、時価総額で最大の暗号資産は低いボラティリティ（価格変動）を維持した。コインメトリクス（Coin Metrics）によると8月下旬以降、30日ボラティリ',
              url:
                'https://news.yahoo.co.jp/articles/3b2fb897f5ff3fb5410b6e163571c99724b984e7',
              urlToImage:
                'https://amd-pctr.c.yimg.jp/r/iwiz-amd/20201119-00108430-coindesk-000-1-view.jpg',
              publishedAt: '2020-11-19T02:00:03Z',
              content:
                'Alameda ResearchSam Trabucco\r\nGlassnode111698201712\r\nGreymatter CapitalJoseph Todaro\r\n20162\r\nThree Arrows CapitalKyle Davies\r\nCoinDesk JapanSource: Coin Metrics, CoinDesk ResearchTraders Brace for Ma… [+49 chars]',
            },
            {
              source: {id: null, name: 'CNA'},
              author: 'CNA',
              title:
                "Analysis: Another bitcoin bubble? This time it's different, backers hope",
              description:
                'With bitcoin surging to the cusp of its 2017 all-time high, backers are hoping fewer frenzied retail investors means less chance of a crash this time around.',
              url:
                'https://www.channelnewsasia.com/news/business/analysis--another-bitcoin-bubble--this-time-it-s-different--backers-hope-13590746',
              urlToImage:
                'https://cna-sg-res.cloudinary.com/image/upload/q_auto,f_auto/image/13590744/16x9/991/557/3a47c7c81dc0a35381e966d9fcb8096b/he/file-photo--virtual-currency-bitcoin-tokens-are-seen-in-this-illustration-picture-2.jpg',
              publishedAt: '2020-11-19T01:50:27Z',
              content:
                'LONDON: With bitcoin surging to the cusp of its 2017 all-time high, backers are hoping fewer frenzied retail investors means less chance of a crash this time around.\r\nBut with little mainstream usage… [+3993 chars]',
            },
            {
              source: {id: null, name: 'Yahoo Entertainment'},
              author: 'Tom Wilson and Anna Irrera',
              title:
                "Analysis: Another bitcoin bubble? This time it's different, backers hope",
              description:
                'With bitcoin surging to the cusp of its 2017 all-time high, backers are hoping fewer frenzied retail investors means less chance of a crash this time around....',
              url:
                'https://finance.yahoo.com/news/analysis-another-bitcoin-bubble-time-014752216.html',
              urlToImage:
                'https://s.yimg.com/uu/api/res/1.2/k10.NCKYt9ZSNqmjL7S3_w--~B/aD00NjA7dz04MDA7YXBwaWQ9eXRhY2h5b24-/https://media.zenfs.com/en-US/reuters.com/5208e6828f4e2dd8b25b787a4941d369',
              publishedAt: '2020-11-19T01:47:52Z',
              content:
                'By Tom Wilson and Anna Irrera\r\nLONDON (Reuters) - With bitcoin surging to the cusp of its 2017 all-time high, backers are hoping fewer frenzied retail investors means less chance of a crash this time… [+3931 chars]',
            },
            {
              source: {id: null, name: 'Realvision.com'},
              author: 'Real Vision',
              title: 'Tesla, Banks, Bonds, and Bitcoin: DB - Nov18, 2020',
              description:
                'Real Vision Managing editor Ed Harrison hosts Tommy Thornton, founder of Hedge Fund Telemetry, to discuss the opportunities and risks on the investment horizon. Tommy explores whether the ongoing rotation into value will continue, sharing his views on sectors…',
              url:
                'https://www.realvision.com/podcast/realvision/episode/7048a64c-1ae3-11eb-ab1a-af2233f95148',
              urlToImage: null,
              publishedAt: '2020-11-19T01:31:00Z',
              content:
                'Tesla, Banks, Bonds, and Bitcoin: DB - Nov18, 2020\r\nReal Vision Managing editor Ed Harrison hosts Tommy Thornton, founder of Hedge Fund Telemetry, to discuss the opportunities and risks on the invest… [+638 chars]',
            },
          ],
        },
      });
    }, 3000);
  });

  // return axios({
  //   method: 'GET',
  //   url:
  //     'http://newsapi.org/v2/everything?q=bitcoin&from=2020-10-19&sortBy=publishedAt&apiKey=e938aafe3d0f4768bef4b2109a2ac3b9',
  // });
}
