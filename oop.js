class Component {
    constructor(roda, warna, kecepatan){
        this.roda= roda;
        this.warna= warna;
        this.kecepatan= kecepatan;
    }

    setKecepatan(newKecepatan) {
        this.kecepatan = newKecepatan;
    }
}

class Kendaraan extends Component {
    constructor(roda, warna, kecepatan, pabrikan){
        super(roda, warna, kecepatan);
        this.pabrikan= pabrikan;
        
    }
    printMaju(){
        console.log ("kecepatan kendaraan ini adalah", this.kecepatan)
    }
}

const Motor = new Kendaraan('4', 'Silver', '90', 'Honda')
Motor.printMaju();
console.log(Motor);
